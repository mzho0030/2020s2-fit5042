
package fit5042.tutex.calculator;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.CreateException;
import javax.ejb.Stateful;
import fit5042.tutex.repository.entities.Property;

@Stateful
public class ComparePropertySessionBean implements CompareProperty{
	
	private Set<Property> list;
    
    public ComparePropertySessionBean() {
    	list = new HashSet<>();
    }
    
    // Add property to the list
    @Override
    public void addProperty(Property property) {
        list.add(property);
    }
    
    // Remove property from list
    @Override
    public void removeProperty(Property property) {
        for (Property p : list) {
        	if (p.getPropertyId() == property.getPropertyId()) {
        		list.remove(p);
        		break;
        	}
        }
    }    

    // Find which one is the best one - the lowest price per room
    @Override
    public int getBestPerRoom() {
        Integer bestId = 0;
        int numberOfRooms;
        double price;
        double bestPerRoom = 99999999.00;
        for(Property p : list)
        {
            numberOfRooms = p.getNumberOfBedrooms();
            price = p.getPrice();
            if(price / numberOfRooms < bestPerRoom)
            {
                bestPerRoom = price / numberOfRooms;
                bestId = p.getPropertyId();
            }
        }
        return bestId;
    }

    // Throw CreateException and RemoteException
    @PostConstruct
    public void init() {
        list = new HashSet<>();
    }

    public CompareProperty create() throws CreateException, RemoteException {
        return null;
    }

    public void ejbCreate() throws CreateException {
    }
}
